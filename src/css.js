export const colours = {
  qantasRed: '#E40000',
  qantasRedGradient: '#F70500',
  qantasRedEarth: '#D20000',
  qantasWarning: '#ED710B',
  qantasWarningHover: '#FCEBCD',
  qantasWhite: '#FFFFFF',
  qantasVeryLightGrey: '#FAFAFA',
  qantasLightGrey: '#DADADA',
  qantasGrey: '#555555',
  qantasDarkGrey: '#D4D4D4',
  qantasDarkerGrey: '#333333',
  qantasCharcoal: '#323232',
  qantasSteel: '#4A4A4A',
  qantasIceWhite: '#F5F5F5',
  qantasLightBlue: '#90e2df',
  qantasAqua: '#BFF5F3',
  qantasLightAqua: '#D8EFEE',
  qantasGray91: '#E8E8E8',
  qantasLightGreen: '#4d8b38',
  qantasLightAmber: '#f3740c',
  qantasListBorder: '#979797',
  qantasListTab: '#f7f7f7',
  qantasLightCyan: '#8de2e0',
  qantasAquaqantasGrey: '#dddddd',
  webChatShadow: 'rgba(0,0,0,.35)',
  calloutGrey: '#d1d1d1'
}

export const fontSizes = {
  xxxl: '1.75rem',
  xxl: '1.5rem',
  xl: '1.25rem',
  large: '1.125rem',
  base: '1rem',
  small: '.875rem',
  xs: '.75rem',
  xxs: '.5rem',
  xxxs: '.375rem'
}

export const customFonts = {
  qsIcons: 'qs-icons',
  ciutadellaReg: 'Ciutadella-Regular',
  ciutadellaMed: 'Ciutadella-Medium'
}

export const layoutSizes = {
  containerWidth: '970px',
  containerWideWidth: '1170px',
  desktopWidth: '769px',
  ipadPortraitWidth: '991px',
  mobileWidth: '581px'
}

export const gradients = {
  greyToWhite: 'linear-gradient(to left, #ffffff, #e8e8e8);'
}

export const responsiveQuery = {
  desktop: '(min-width: 768px)',
  mobile: '(max-width: 767px)',
  tablet: '(min-width: 581px) and (max-width: 990px)',
  mobileAndTablet: '(max-width: 990px)',
  tabletAndDesktop: '(min-width: 991px)'
}

export const globalStyles = `
  @font-face {
    font-family: 'Ciutadella-Regular';
    src: url('/static/fonts/Ciutadella-Regular.eot');
    src: url('/static/fonts/Ciutadella-Regular#iefix') format('embedded-opentype'),
        url('/static/fonts/Ciutadella-Regular.woff') format('woff'),
        url('/static/fonts/Ciutadella-Regular.ttf') format('truetype');
    font-display: fallback;
  }

  @font-face {
    font-family: 'Ciutadella-Medium';
    src: url('/static/fonts/Ciutadella-Medium.eot');
    src: url('/static/fonts/Ciutadella-Medium#iefix') format('embedded-opentype'),
        url('/static/fonts/Ciutadella-Medium.woff') format('woff'),
        url('/static/fonts/Ciutadella-Medium.ttf') format('truetype');
    font-display: fallback;
  }

  @font-face {
    font-family: 'qs-icons';
    src: url('/static/fonts/qs-icons.eot');
    src: url('/static/fonts/qs-icons.eot#iefix') format('embedded-opentype'),
        url('/static/fonts/qs-icons.woff') format('woff'),
        url('/static/fonts/qs-icons.ttf') format('truetype');
    font-display: fallback;
  }

  html, body {
    width: 100%;
    height: 100%;
  }

  body {
    margin: 0;
    font-family: ${customFonts.ciutadellaReg}, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
    -webkit-font-smoothing: antialiased;
    overflow-x: hidden;

    & > div:first-child {
      height: 100%;
    }

    * {
      box-sizing: border-box;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }
  }

  main {
    width: 100%;
    margin: 0 auto;
    position: relative;
    padding: 0;
  }

  .content {
    height: 100%;

  }

  .sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
  }

  a {
    color: ${colours.qantasRed};
  }

  #enrolment-form {
    width: 100%;
    min-height: 660px;
    overflow: hidden;
    border: none;

    @media only screen and (min-width: ${layoutSizes.desktopWidth}) {
      width: 80%;
    }

    .section-body {
      padding: 2rem;
      background: ${colours.qantasWhite};
    }

    .enrollment-widget-result-container {
      width: 400px;
      min-height: 300px;
      margin: auto;
      padding: 2rem 0;

      .enrollment-widget-header {
        display: flex;
        align-items: center;
        margin-bottom: 1rem;
        .enrollment-instructions {
          padding-left: 1rem;
          p {
            font-size: ${fontSizes.base};
            margin: 0;
          }
        }
      }
      .enrollment-widget-result {
        text-align: center;
        p {
          font-size: ${fontSizes.base};
        }
      }
    }

    .enrollment-terms-container {
      .terms-title {
        padding: 0;
        background: ${colours.qantasWhite};
        text-align: center;

        a {
          display: block;
          margin: 0;
          padding: 0.5rem 1rem;
          background: ${colours.qantasWhite};
          color: ${colours.qantasCharcoal};
          font-size: ${fontSizes.small};
          font-weight: 500;
          text-transform: uppercase;
          border-radius: 4px;
          z-index: 4;
          box-shadow: 0 2px 2px 0 rgba(0,0,0,0.1);
        }

        i {
          font-size: 6px;
          margin-left: 10px;
          &:before {
            margin-top: -0.4rem;
          }
        }
      }
      .enrollment-terms {
        background-color: ${colours.qantasWhite};
        padding: 0 2.75rem;
      }
    }
}
`
