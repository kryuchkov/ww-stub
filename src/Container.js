import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { layoutSizes } from './css'


const Wrapper = styled.div`
  max-width: ${layoutSizes.containerWidth};
  margin: 0 auto;
  ${props => props.wideView && `
    max-width: ${layoutSizes.containerWideWidth};
  `}
  position: relative;
  padding-left: 15px;
  padding-right: 15px;
`

const Container = (props) => {
  return (
    <Wrapper
      {...props}
    >
      {props.children}
    </Wrapper>
  )
}

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
}

export default Container
