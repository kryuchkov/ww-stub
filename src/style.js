import styled from 'styled-components'
import { 
  colours, 
  fontSizes, 
  responsiveQuery, 
  customFonts,
  layoutSizes
} from './css'
import error from './ico_error.svg'
import successImg from './success-image.png'
import Container from './Container'

export const SuccessBox = styled.div`
  border-radius: 4px;
  box-shadow: 0 1px 5px 0 #00000080;
  min-height: 240px;
  max-width: 748px;
  margin: 1.5rem auto 3rem;
  position: relative;
  
  @media only screen and ${responsiveQuery.desktop} {
    display: flex;
  }
  
  > svg {
    display: none;
    
    @media only screen and ${responsiveQuery.desktop} {
      display: block;
      position: absolute;
      top: -24px;
      right: -24px;
    }
  }
`

export const CopyBlock = styled.div`
  padding: 3rem 1rem 1rem;
  text-align: left;
  position: relative;
  
  @media only screen and ${responsiveQuery.desktop} {
    width: 50%;
  }
  
  h3 {
    font-size: ${fontSizes.xxl};
    margin: 0 0 1rem;
  }
  
  svg {
    width: 65px;
    position: absolute;
    top: -27%;
    left: 40%;
    
    @media only screen and ${responsiveQuery.desktop} {
      transform: translate(-70%, 15px);
      width: auto;
      position: relative;
      top: -0.5rem;
      left: inherit;
    }
  }
`
const Box = styled.div`
  margin: auto;
  padding: 1rem 2rem; 
  border-radius: 4px;
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.2), 0 15px 14px 0 rgba(0, 0, 0, 0.05);
  background-color: ${colours.qantasWhite};
  font-size: ${fontSizes.small};
`

export const ErrorBox = styled(Box)`
  max-width: 300px;
  background-color: #f9f3e9;
  margin: 1rem auto;
  
  i {
    margin: auto;
  }
`

export const ErrorIcon = styled.i`
  display: block;
  height: 35px;
  width: 40px;
  background: url('${error}') no-repeat;
  background-size: contain;
`
export const ErrorInfo = styled.p``

export const ErrorText = styled.div`
  margin-top: 0.5rem;
  font-family: ${customFonts.ciutadellaMed};
  font-size: ${fontSizes.large};
`

export const ImageBlock = styled.div`
  background: url('${successImg}') no-repeat;
  background-size: cover; 
  height: 152px;
  
  @media only screen and ${responsiveQuery.desktop} {
    width: 50%;
    height: auto;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  }
`
export const SuccesssWrapper = styled(Container)`
  text-align: center;
  
  h2 {
    font-size: 1.625rem;
    font-weight: normal;
    font-family: ${customFonts.ciutadellaMed};
  }
  input {
    right: .75rem;
    top: .75rem;
    border: 0.125rem solid rgba(255, 255, 255, 0.2);
    border-color: ${colours.qantasLightAqua};
    vertical-align: middle;
    height: 40px;
    width: 100px;
  }
`

export const Wrapper = styled.div`
  width: 100%;
  text-align: center;
  padding-top: 1rem;
  
  > p {
    padding: 0 1rem;
    max-width: 400px;
    margin: 0 auto;
  }
`

export const FrameWrapper = styled.div`
  background-color: ${colours.qantasIceWhite};
  margin-top: 1.5rem;
  
  @media only screen and (min-width: ${layoutSizes.desktopWidth}) { 
    padding: 0 2rem;
  }
`

export const IFrame = styled.iframe`
  width: 100%;
  min-height: 810px !important;  
  overflow: hidden;  
  border: none;
`

export const ResultWrapper = styled.div`
  padding: 1rem;
`

export const ActivatingText = styled.p`
  text-align: center;
  margin-top: 0;
`
