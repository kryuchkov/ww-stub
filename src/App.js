/** @format */

import React, {useEffect, useState, useCallback} from 'react'
import {Success} from './Success'
import {Login} from './Login'

const initialState = {
  done: false,
  showTimeout: false,
  widgetLoaded: true,
  message: null,
  showError: false,
  showOutage: false,
  memberId: null,
  event: {},
  type: '',
}

const getQueryVariable = variable => {
  var query = window.location.search.substring(1)
  console.log(query) //"app=article&act=news_content&aid=160990"
  var vars = query.split('&')
  console.log(vars) //[ 'app=article', 'act=news_content', 'aid=160990' ]
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=')
    console.log(pair) //[ 'app', 'article' ][ 'act', 'news_content' ][ 'aid', '160990' ]
    if (pair[0] === variable) {
      return pair[1]
    }
  }
  return false
}

const Progress = props => {
  const [state, setState] = useState(initialState)
  const done = () => {
    if (state.event)
      state.event.source.postMessage(
        Object.assign({done: true}, state.type === 'login' ? {loggedin: true} : {linked: true}),
        state.event.origin,
      )
    setState({done: true})
  }

  const messageHandler = useCallback(e => {
    if (!e.data) {
      return
    }

    try {
      const message = (e => {
        try {
          return JSON.parse(e.data)
        } catch (_) {
          return e.data
        }
      })(e)

      if (message.memberId) {
        setState({
          event: e,
          memberId: message.memberId,
          type: 'link',
        })
        e.source.postMessage({finishedLoading: true}, e.origin)
      }

      if (message.login) {
        setState({event: e, type: 'login'})
        e.source.postMessage(
          {
            finishedLoading: true,
            scrollHeight:
              (!window.chrome && document.documentElement.scrollHeight) ||
              document.body.scrollHeight,
            scrollWidth:
              (!window.chrome && document.documentElement.scrollWidth) || document.body.scrollWidth,
          },
          e.origin,
        )
      }

      if (message.confirm) {
        setState({event: e, type: 'confirm'})
        window.setTimeout(
          () =>
            e.source.postMessage(
              {
                confirmed: true
              },
              e.origin,
            ),
          5000,
        )
      }
    } catch (err) {
      e.source.postMessage({error: (err && err.message) || err}, e.origin)
      setState({
        ...initialState,
        showError: true,
        message: (err && err.message) || err,
        widgetLoaded: false,
      })
    }

    return () => window.removeEventListener('message', messageHandler, false)
  }, [])

  useEffect(() => {
    window.addEventListener('message', messageHandler, false)
    //setState({ type: props.ct || getQueryVariable("ct") });
    return () => {
      window.removeEventListener('message', messageHandler, false)
    }
  }, [messageHandler, props])

  return (
    <>
      {state.type === 'link' && state.done && (
        <div>You are now earning Qantas Points with Woolworths Rewards</div>
      )}
      {state.type === 'link' && !state.done && <Success done={done} memberId={state.memberId} />}
      {state.type === 'login' && !state.done && <Login done={done} />}
      {state.showError && <div>state.message || 'Error occured' </div>}
    </>
  )
}

export default Progress
