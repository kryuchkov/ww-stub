import React from 'react'
import styled, { keyframes } from 'styled-components'
import { 
  colours, 
  fontSizes, 
  customFonts, 
  layoutSizes 
} from './css'

const ButtonContainer = styled.button`
  font-family: inherit;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  user-select: none;
  border: 2px solid transparent;
  border-radius: 0.25rem;
  padding: 0.75rem 1rem;
  font-size: ${fontSizes.small};
  border-radius: ${props => props.rounded ? '1.375rem' : '0.25rem'};
  transition: all 0.2s ease-in-out;
  text-transform: uppercase;
  letter-spacing: 2px;
  position: relative;
  line-height: 1.4;
  font-family: ${customFonts.ciutadellaMed};

  ${props => props.primary && `  
    background: ${colours.qantasRed};
    color: ${colours.qantasWhite};  
    &:hover, &:active {
      background-color: ${colours.qantasRedEarth};
      border-color: ${colours.qantasRedEarth};
    }
  `}

  ${props => props.outline && `
    background: ${colours.qantasWhite};
    color: ${props.secondary ? colours.qantasCharcoal : colours.qantasRed};
    border-color: currentColor;
    
    &:hover, &:active {
      background-color: ${props.secondary ? colours.qantasCharcoal : colours.qantasRed};
      border-color: ${props.secondary ? colours.qantasCharcoal : colours.qantasRed};
      color: ${colours.qantasWhite}
    }
  `}

  ${props => props.disabled && `
    cursor: not-allowed;
    opacity: 1;
    background-color: ${colours.qantasLightGrey};
    color: ${colours.qantasCharcoal};
    border: 2px solid ${colours.qantasLightGrey};
    
    &:hover, &:active {
      background-color: ${colours.qantasLightGrey};
      color: ${colours.qantasCharcoal};
      border: 2px solid ${colours.qantasLightGrey};
    }
  `}

  ${props => props.link && `
    background-color: transparent;
    text-decoration: none;
  `}
  
  ${props => props.block && `
    width: 100%;
    max-width: 100%;
  `}
  
  ${props => props.narrow && `
    height: auto;
    
    @media only screen and (min-width: ${layoutSizes.desktopWidth}) {
      padding: 0 1rem;
      height: 33px;
    }
  `}
`

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`

const ButtonLoader = styled.span`
  display: inline-block;
  position: absolute;
  right: .75rem;
  top: .75rem;
  border: 0.125rem solid rgba(255, 255, 255, 0.2);
  border-left-color: ${colours.qantasWhite};
  -webkit-animation: ${rotate360} 1.1s infinite linear;
  animation: ${rotate360} 1.1s infinite linear;
  border-radius: 50%;
  width: 18px;
  height: 18px;
  vertical-align: middle;
`

const Button = (props) => {
  return (
    <ButtonContainer
      onClick={props.onClick}
      {...props}
    >
      {props.children}
      {props.loading && <ButtonLoader />}
    </ButtonContainer>
  )
}


export default Button
